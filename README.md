![KILL ALL](https://i.imgur.com/uZTRX6g.png)
# Robot BT (BOEBOI TAPOK)
version 2.0
Cross-Browser, multi-platform tapokkiller. Server for controlling movement of robot. 

# Features!

  - Online Wi-Fi remote control!
  - Static camera!
  - Photo saving!


You can also:
  - Kill tapki.
  - Menyat batareiki.

### Installation

Project developed using [Node.js](https://nodejs.org/) v6.114.

Install the dependencies and devDependencies and start the server.

```sh
$ npm install -d
```

### Development
Want to contribute? Great!
Open your favorite Terminal and run these commands.

For starting app enter
```
$ node bin/www
```
On tapokkiller Node.js server starting with raspberry-pi and before runnig execution need to stop node proccess from terminal.

### Deployment

Connect to Wi-Fi or by the Ethernet cabel. Actual network information for 13.12.2017:
- ssid: RasspberryTank 
- password: 10610600

For login use defualt user (pi) and password (raspberry)
```
$ ssh pi@192.168.0.1
$ raspberry
```

Open project directory
```
$ cd nodeServer/
```

Tip: use [SFTP](!https://habrahabr.ru/post/89473/) for file synchronization.



### Todos

 - Camera moving
 - Add Night camera

### Authors
- Slava 1
- [Slava 2](!https://vk.com/form0)

License
----
Copyright 2017 KOLESNIKOV

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**Free Software, Hell Yeah!**
