const ws = new WebSocket('ws://' + location.host);

window.onload = function () {
    let streamFrame = document.createElement("img");
    streamFrame.src = "http://" + location.hostname + ":8080/?action=stream";
    streamFrame.className = 'streamFrame';

    let streamContainer = document.getElementById('stream_container');
    streamContainer.appendChild(streamFrame);

    streamContainer.href = "http://" + location.hostname + ":8080/?action=snapshot"
};

window.oncontextmenu = function(event) {
    event.preventDefault();
    event.stopPropagation();
    return false;
};

ws.onmessage = function (p1) {
    console.log(p1);
    let messageContainer = document.getElementById('message_container');
    let message = document.createElement('p');
    message.innerHTML = p1.data;
    messageContainer.appendChild(message)
};

ws.onopen = function (event) {
    console.log("connected ok: " + event);
};

ws.onerror = function (event) {
    console.log("error: " + event);
};

ws.onclose = function (p1) {
    console.log("on close: " + p1);
};

let keysDict = {
    'ArrowUp': {
        action: 'move',
        number: '0'
    },
    'ArrowDown': {
        action: 'move',
        number: '1'
    },
    'ArrowLeft': {
        action: 'move',
        number: '2'
    },
    'ArrowRight': {
        action: 'move',
        number: '3'
    },
    'w': {
        action: 'move',
        number: '0'
    },
    's': {
        action: 'move',
        number: '1'
    },
    'a': {
        action: 'move',
        number: '2'
    },
    'd': {
        action: 'move',
        number: '3'
    },
};

document.onkeydown = function (event) {
    let params = keysDict[event.key];
    if (!params) return;

    sendAction(params.action, params.number)

};

document.onkeyup = function (event) {
    sendAction('stop')
};

function sendAction(id, param) {
    let action = {};
    action.id = id;
    action.param = param;
    ws.send(JSON.stringify(action));
}
