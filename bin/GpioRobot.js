const FORWARD = 0;
const BACKWARD = 1;
const LEFT = 2;
const RIGHT = 3;
const rpio = require('rpio');

class GpioRobot {

    constructor(PIN0, PIN1, PIN2, PIN3) {
        const options = {
            gpiomem: false,
            mapping: 'physical',
        };

        rpio.init(options);

        this.left = PIN2;
        this.right = PIN1;
        this.bLeft = PIN3;
        this.bRight = PIN0;

        console.log(this);

        rpio.open(this.left, rpio.OUTPUT, 1);
        rpio.open(this.right, rpio.OUTPUT, 1);
        rpio.open(this.bLeft, rpio.OUTPUT, 1);
        rpio.open(this.bRight, rpio.OUTPUT, 1);
    }

    move(direction) {
        this.stop();
        switch (direction) {
            case BACKWARD: {
                rpio.write(this.left, 0);
                rpio.write(this.right, 0);
                break;
            }
            case FORWARD: {
                rpio.write(this.bLeft, 0);
                rpio.write(this.bRight, 0);
                break;
            }
            case LEFT: {
                rpio.write(this.left, 1);
                rpio.write(this.right, 0);
                break;
            }
            case RIGHT: {
                rpio.write(this.left, 0);
                rpio.write(this.right, 1);
                break;
            }
        }
    }

    stop() {
        rpio.write(this.left, 1);
        rpio.write(this.right, 1);
        rpio.write(this.bLeft, 1);
        rpio.write(this.bRight, 1);
    }
}

module.exports = GpioRobot;
