let WebSocketServer = require('ws').Server;
let GpioRobot = require('./GpioRobot');

class ControlServer {

    start(hostServer) {
        let robot = new GpioRobot(11, 13, 15, 19);

        let webSocketServer = new WebSocketServer({server: hostServer});
        console.log("started");

        webSocketServer.on('connection', function (ws) {
            console.log('client connected');

            ws.on('message', function (message) {
                try {
                    let action = JSON.parse(message);
                    console.log("action: " + message);
                    switch (action.id) {
                        case 'move': {
                            robot.move(parseInt(action.param));
                            break
                        }
                        case 'stop': {
                            robot.stop();
                            break
                        }
                        default: {
                            ws.send("unknown action: " + action.id)
                        }
                    }
                } catch (e) {
                    ws.send("error: " + e.message)
                }
            });

            ws.on('close', function () {
                robot.stop();
            })
        });
    }
}

module.exports = ControlServer;


